from django.shortcuts import render
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()

    form = StatusForm()
    statusList = Status.objects.all()

    return render(request, 'index.html', {'form': form, 'statusList': statusList})
