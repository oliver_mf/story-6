from django.urls import include, path
from . import views
from django.contrib import admin

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
]