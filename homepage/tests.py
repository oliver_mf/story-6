from django.test import TestCase
from django.test.client import Client
from django.urls import resolve
from .models import Status
from .forms import StatusForm
from .views import index
from django.apps import apps
from homepage.apps import HomepageConfig

# Create your tests here.
class story6Tests(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_function(self):
        found = resolve('/')
        self.assertEqual(found.func.__name__, index.__name__)

    def test_create_new_status(self):
        new = Status.objects.create(status = 'mantap')
        self.assertTrue(isinstance(new, Status))
        self.assertTrue(new.__str__(), new.status)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_display_status(self):
        status = "mantap"
        data = {'stats' : status}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code, 200)

    def test_application(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_hello(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello", response_content)

    def test_valid_post(self):
        response = self.client.post('', {'status':'mantap'}, follow = True)
        response_content = response.content.decode('utf-8')
        self.assertIn('mantap', response_content)

    def test_invalid_post(self):
        form = StatusForm({'status':''})
        self.assertFalse(form.is_valid())