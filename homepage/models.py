from django.db import models
from django.forms import ModelForm

#Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=255, primary_key=True)

    def __str__(self):
        return self.status

